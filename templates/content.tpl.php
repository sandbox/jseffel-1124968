<?php
// $Id$
/**
 * @file
 * @ingroup contentbrowser
 * @brief
 *  Generic template file.
 * Params available:
 *  - $node - The node object
 *  - $tag - The decoded tag.
 *  - $attachment - FALSE or Array with file information.
 *  - $from_editor - TRUE if we are called from an editor. It is not safe to render embed-code, javascript etc...
 *  - $caption_data - The text under the image (not CCK fields)
 */

$out = theme('contentbrowser_insertion', $node, $tag, $attachment, $from_editor);

if((isset($tag['insertion']) && !empty($tag['insertion']) && preg_match("/(inline|link|collapsed|lightbox)/", $tag['insertion'], $matches))) {
 $caption_title = '';
}

if(empty($tag['insertion']) && empty($out)) {
  //Act as a default handler for content which has no insertion type
  //This is rather lame, it really should exist a better template.
  $out = node_view($node, FALSE, TRUE, FALSE);
}

$caption_data = array_filter(array_merge(array($caption_title), $caption_data));
if(count($caption_data) && !$from_editor) {
  $out .= "<span class='contentbrowser_caption'>" . implode("<br />", $caption_data) . "</span>";
}

print $out;
?>
