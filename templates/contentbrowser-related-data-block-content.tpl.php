<?php
// $Id$
/**
 * @file
 * @ingroup contentbrowser
 */
 
$types = node_get_types();
if(module_exists('creativecommons_lite')) {
  $cc_types = creativecommons_lite_get_license_array(variable_get('creativecommons_lite_license_options', NULL));
}
else {
  $cc_types = array();
}
foreach($nids as $nid_object) {
  $node = node_load($nid_object->nid);
  print l($node->title, "node/".$node->nid)." (".$types[$node->type]->name. ")<br />";
  $creative_commons = @$cc_types[$node->cc_lite_license];

  //No need to check if the creativecommons module is enabled - if the cc_lite_license
  //variable is set it clearly must be enabled
  if(isset($node->cc_lite_license) && !empty($node->cc_lite_license)) {
    print strip_tags(get_license_html($node->cc_lite_license), "<a><img>");
  }
  
  print "<br />";
  print '<br />';
}
?>
