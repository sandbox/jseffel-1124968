<?php
// $Id$
/**
 * @file
 * @ingroup contentbrowser
 */
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <?php print $css; ?>
  <?php print $js;  ?>

</head>
<body onload='fireSearch()'>
  <div id='contentbrowser'>
    <?php print $form; ?>
    <div id='search-results-wrapper' class='form-item'>
    </div>
  </div>
  
  <script type='text/javascript'>
  function fireSearch() {
    var page = <?php print (isset($_REQUEST['q']) && preg_match("/page/", $_REQUEST['q'], $matches)) ? 1 : 0; ?>;
    if(($('#contentbrowser_id') && $('#contentbrowser_id').val() && $('#contentbrowser_id').val().length) || (page)) {
      $('#search_button').click();
      //If we have selected a element in the contentbrowser, remove the input
      //which contains its nid after the first search.
      $('#searched_nid').remove();
    }
  }  
  </script>
</body>
</html>