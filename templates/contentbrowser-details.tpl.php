<?php
// $Id$
/**
 * @file
 * @ingroup contentbrowser 
 * @brief
 *  The template for the detailed node view.
 *
 * Parameters available:
 *  - $thumbnail
 *  - $node
 *  - $form
 */
$types = node_get_types();
?>

<div id='contentbrowser_wrapper'>
  <div id='contentbrowser_node_details'>
    <?php if($thumbnail): ?>
    <p><img src='<?php print $thumbnail;?>' /></p>
    <?php endif; ?>
    <p><?php print t('Title') . ": " . $node->title; ?></p>
    <p><?php print t('Type') .  ": " . $types[$node->type]->name; ?></p>
    <p><?php print t('Author') . ": " . $node->name; ?></p>
    <p><?php print t('Created') . ": " . format_date($node->created, 'small'); ?></p>
  </div>
  
  <div id='contentbrowser_details_form'>
    <?php print theme('status_messages'); ?>
    <?php print $form; ?>
  </div>
  <div style='clear: both'></div>
</div>

<script type='text/javascript'>
  Drupal.behaviors.autocomplete();
  Drupal.behaviors.collapse();
</script>
