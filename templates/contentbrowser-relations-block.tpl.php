<?php
// $Id$
/**
 * @file
 * @ingroup contentbrowser
 * @brief
 *  File for rendering the relations block.
 *
 * Variables available:
 *  - $nodes
 */
if(count($nodes)):?>
<div class="relatedContent">
<?php
  $types = node_get_types();
  print "<ul class='contentbrowser_relations'>";
  foreach($nodes as $node) {
    $lang = empty($node->language) ? '' : ", " . $node->language;
    $node_type = empty($types[$node->type]->name) ? $node->type : $types[$node->type]->name;
    print "<li>". l($node->title, 'node/'.$node->nid) . " (" . $node_type . $lang . ")" . "</li>";
  }
  print "</ul>";
?>
</div>
<?php endif; ?>
