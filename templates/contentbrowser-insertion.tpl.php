<?php
// $Id$
/**
 * @file
 * @ingroup contentbrowser
 * @brief
 *  Template file for rendering the type if insertion.
 *
 * Parameters available:
 *  - $node
 *  - $tag
 *  - $from_editor
 */

global $base_url;

//Dont allow body/teaser insertion in the TinyMCE editor.
if($from_editor && ($tag['insertion'] == 'teaser' || $tag['insertion'] == 'body' || in_array($tag['insertion'], array('collapsed_teaser', 'collapsed_body')))) {
  print t('Node with nid #%nid (%type) will be inserted here', array('%nid' => $node->nid, '%type' => $node->type));
  return;
}

$link_to_content = "node/".$node->nid;
$lightbox_link_to_content = url("contentbrowser/node/".$node->nid);
$teaser_type = FALSE;
$out = '';

if(module_exists('ndla_content_translation')) {
  if(ndla_content_translation_enabled($node->type)) {
    $old_title = $node->title;
    ndla_content_translation_view($node, $teaser_type, FALSE, TRUE);
    $node->body = $node->content['body']['#value'];
    $node->teaser = $node->field_ingress[0]['safe'];
    //If the title isnt translated, use the title which has been entered by the user.
    if($old_title != $node->title && $old_title == $tag['link_text']) {
      $tag['link_text'] = $node->title;
    }
  }
}

//We have a file attachment which we wuold like to display instead of the node.
if($attachment) {
  $link_to_content = file_create_url($attachment['filepath']);
  $lightbox_link_to_content = $base_url."/".$attachment['filepath'];
}

$title = $lightbox_title = (isset($tag['link_text']) && !empty($tag['link_text'])) ? $tag['link_text'] : $node->title;

//The template content-flashnode sends a custom title to be used. Make a check and see if it exists.
if(isset($custom_title) && !empty($custom_title)) {
  //We have a winner. Override the title
  $title = $custom_title;
}

if(in_array($tag['insertion'], array('collapsed_teaser', 'collapsed_body')) && !$attachment) {
  $text = ($tag['insertion'] == 'collapsed_teaser') ? $node->teaser : $node->body;
  $out = theme('fieldset', array('#collapsible'=>true, '#collapsed'=>true, '#title'=> $title, '#value' => $text));
}
else if($tag['insertion'] == 'title' && !$attachment) {
  $out = $node->title;
}
else if($tag['insertion'] == 'teaser' && !$attachment) {
  $out = $node->teaser;
}
else if($tag['insertion'] == 'body' && !$attachment) {
  $out = $node->body;
}
else if(strpos($tag['insertion'], 'lightbox') !== FALSE || $tag['link_type'] == 'lightbox') {
  $lightbox_data = _contentbrowser_get_fields($node, array_filter(variable_get('contentbrowser_lightbox_'. $node->type .'_fields', array())), $tag['remove_fields_lightbox'], 'lightbox');
  $lightbox_data = str_replace("\n", "", implode(" | ", array_filter($lightbox_data)));
  $lightbox_data = htmlentities($lightbox_data, ENT_COMPAT, "UTF-8");
  $width_height = _contentbrowser_get_lightbox_dimensions($tag);
  $more_info = l(t('More information'), 'node/' . $node->nid);
  $out = "<a href='".$lightbox_link_to_content."' rel='lightframe[|$width_height][$lightbox_title<br />$lightbox_data" . "<br />$more_info]'>$title</a>";
}
else if($tag['insertion'] == 'link' || $attachment) {
  $out = l($title, $link_to_content, array('html' => TRUE));
}
else if($tag['insertion'] == 'inline') {
  $out = node_view($node, FALSE, TRUE, FALSE);
}

//Remove div class = full. It messes up our layout
$out = str_replace("class=\"full\"", "", $out);
print $out;
?>
