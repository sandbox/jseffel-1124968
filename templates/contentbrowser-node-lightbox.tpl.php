<?php
// $Id$
/**
 * @file
 * @ingroup contentbrowser
 */
?>
<html>
<head>
<title><?php print $title; ?></title>
<?php print drupal_get_css(); ?>
<?php print drupal_get_js('header'); ?>
</head>

<body class="lightboxContent">
<h1><?php print $title; ?></h1>
<div id="content">
<div id="body">
<?php print $content; ?>
</div>
</div>
</body>
</html>