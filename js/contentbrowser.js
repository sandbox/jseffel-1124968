// $Id$
/**
 * @file
 * @ingroup contentbrowser
 */
 
/**
 * Loops through all items in the form which has the postable class
 * and creates a tag from tag.
 */
function contentbrowser_get_tag() {
  if(contentbrowser_validate_form()) {
    var data = new Array();
    $(".postable").each( function() {      
      if($(this).is('input:checkbox')) {
        var check_value = $(this).attr('checked') ? 1 : 0;
        data.push($(this).attr('name') + "=" + check_value);
      }
      else {
        try {
          data_value = escape($(this).val().replace(/=/g, "\\="));
          data.push($(this).attr('name') + "=" + data_value);
        }
        catch(err) {
          ; //$(this).val() can be empty if CB is misconfigured
        }
      }
    });

    instance = $('#edit-instanceId').val();
    tag = "[contentbrowser ==" + data.join("==") + "]";
    send_to_editor(tag, instance);
  }
}

/**
 * Returns the tag to the TinyMCE editor.
 */
function send_to_editor(tag, instance) {
  tag = new Array(tag);
  //Send an ajax request and get the nice looking HTML.
  $.ajax({
    url: Drupal.settings.basePath + "contentbrowser/decode/editor",
    type: "POST",
    data: {'tags[]': tag},
    dataType: 'json',
    cache: 1,
    async: 0,
    success: function(encoded_tags){
      var html = encoded_tags[0];
      html += "&nbsp;"
    	window.opener.Drupal.wysiwyg.instances[instance].insert(html);
    	window.close();
    }
  });
}

/**
 * Function for validating the form. Returns true on success, false otherwise.
 */
function contentbrowser_validate_form() {
  var error = false;
  var error_message = "";
  
  if($('#edit-fid').val()) {
    var i = $('#edit-insertion').val();
    if(!(i == 'link' || i == 'lightbox_small' || i == 'lightbox_large' || i == 'default')) {
      error_message += (Drupal.t('Only lightbox, link or default are valid insertion options while using attachments.')) + "\n";
      error = true;
    }
  }
  
  if((typeof $('#edit-width').val() != 'undefined') && $('#edit-width').val() != '' && isNaN($('#edit-width').val())) {
    error = true;
    error_message += Drupal.t('Width does not contain a valid value') + "\n";
  }
  if((typeof $('#edit-height').val() != 'undefined') && $('#edit-height').val() != '' && isNaN($('#edit-height').val())) {
    error = true;
    error_message += Drupal.t('Height does not contain a valid value') + "\n";
  }
  
  if($('#edit-insertion').val() == 'lightbox_custom' || $('#edit-link-type').val() == 'lightbox_custom') {
    var size = $('#lightbox_size').val();
    size = size.split("x");
    try {
      width = size[0];
      height = size[1];

      if(isNaN(width) || isNaN(height) || width <= 0 || height <= 0) {
        error = true;
        error_message += Drupal.t("The lightbox size is invalid");
      }
    }
    catch(err) {
      error = true;
      error_message += Drupal.t("The lightbox size is invalid");
    }
  }
  else {
    //Remove any weird things the user might have inserted
    $('#lightbox_size').val('');
  }

  //Validate any items marked with required...
  $(".error").removeClass('error');
  $(".required").each( function() {
      if($(this).val() == '') {
        //Add the error class.
        $(this).addClass('error');
        error = true;
      }
    }
  );
  
  if(error && error_message != '') {
    alert(error_message);
  }
  
  return (!error);
}