<?php
// $Id$
/**
 * Image template file.
 * Params available:
 *  $node - The node object
 *  $tag - The decoded tag.
 * $from_editor
 * $caption_data - The text under the image (not CCK fields)
 */

$out = '';
if($from_editor) {
  $image = _contentbrowser_get_image_thumbnail($tag['nid']);
  // override video width and height from tag
  $img_dim = '';
  if (is_numeric($tag['width'])) {
    $img_dim .= 'width="' . intval($tag['width']). '"';
  }
  if (is_numeric($tag['height'])) {
    $img_dim .= ' height="' . intval($tag['height']). '"';
  }
  
  $im_preset = variable_get('contentbrowser_video_imagecache' ,'');
  if(!empty($im_preset)) {
    $image = imagecache_create_url($im_preset, $image);
  }
  $out = "<img src='" . $image . "' $img_dim />";
}
else {
  // override video width and height from tag
  $params = array();
  if (is_numeric($tag['width'])) {
    $params['width'] = intval($tag['width']);
    //Ignore height, we will calculate the proper height. But we do need the video object in order
    //to know the default width and height. Slightly slower because this will happen inside flashvideo_get_video()
    //aswell.
    $video_object = flashvideo_get_video_object($node);
    $params = _contentbrowser_resize($video_object['width'], $video_object['height'], $tag['width']);
    
  }


  // Override preview image if one has been selected
  if ($node->iids && count($node->iids)) {
    $img_node = node_load($node->iids[0]);
    $img_url = base_path() . file_create_path($img_node->images['_original']);
    $params['flashvars'] = array('image' => $img_url);
  }

  if (count($params) > 0) {
    $out = flashvideo_get_video($node, $params);
  }
  else {
    $out = flashvideo_get_video($node);
  }
}
$caption_data = array_filter(array_merge(array($caption_title), $caption_data));
if(count($caption_data)) {
  //Removed the link to Adobe. We might have a problem here in the future if flashvideo starts to  use p-tags.
  $out = preg_replace("/<p>.*<\/p>/i", "", $out);
  
  $out .= "<span class='contentbrowser_caption'>" . implode("<br />", array_filter($caption_data)) . "</span>";

}
print $out;
?>
