<?php
// $Id$
/**
 * Flashnode template file.
 * Params available:
 *  $node - The node object
 *  $tag - The decoded tag.
 *  $from_editor
 *  $caption_data - The text under the image (not CCK fields)
 */

$out = '';
$caption_data = array_filter(array_merge(array($caption_title), $caption_data));
//If we are coming from the editor AND insertion type is not lightbox (lightbox generates a link which is safe to show in TinyMCE)
if($from_editor && $tag['insertion'] != 'lightbox') {
  $image = contentbrowser_get_thumbnail($tag['nid'], 'flashnode');
  // override flashnode width and height from tag
  $img_dim = '';
  if (is_numeric($tag['width'])) {
    $img_dim .= 'width="' . intval($tag['width']). '"';
  }
  if (is_numeric($tag['height'])) {
    $img_dim .= ' height="' . intval($tag['height']). '"';
  }
  $out = "<img src='" . $image . "' $img_dim />";
}
else {
  // override flashnode width and height from tag
  if (is_numeric($tag['width'])) {
    //Make sure the flashnode object contains width and height else we will see an error (div. by 0)
    if($node->flashnode['width'] && $node->flashnode['height']) {
      $dimensions = _contentbrowser_resize($node->flashnode['width'], $node->flashnode['height'], $tag['width']);
      $node->flashnode['width'] = $dimensions['width'];
      $node->flashnode['height'] = $dimensions['height'];
    }
  }

  if($tag['insertion'] == 'lightbox') {
    $image = _contentbrowser_get_image_thumbnail($node->nid);
    if(empty($image)) {
      $image = NULL;
    }
    else {
      $image = "<img title='" . $node->title . "' src='" . imagecache_create_url($tag['imagecache'], $image) . "' />";
    }
    $out = theme('contentbrowser_insertion', $node, $tag, NULL, NULL, $image);
  }
  else {
    $out = theme('flashnode', $node->flashnode, FALSE);
  }

  if(count($caption_data)) {
    $out .= "<span class='contentbrowser_caption'>" . implode("<br />", array_filter($caption_data)) . "</span>";
  }
}

print $out;
?>
