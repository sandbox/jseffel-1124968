<?php
// $Id$
/**
 * @file
 * @ingroup contentbrowser
 * @brief
 *  Include file for specifying the WYSIWYG plugin.
 */
/**
 * Implementation of hook_wysiwyg_plugin().
 */
function contentbrowser_contentbrowser_wysiwyg_plugin() {
  $plugins['contentbrowser_wysiwyg'] = array(
    'title' => t('Content Browser Plugin'),
    'vendor url' => 'http://cerpus',
    'icon file' => 'contentbrowser.gif',
    'icon title' => 'Content Browser',
    'settings' => array(),
    //TinyMCE specific
    'extended_valid_elements' => array(
      "span[contentbrowser_id|class|style]", 
      "div[contentbrowser_id|class|style]", 
      "embed[*]", 
      "object[*]", 
      "param[*]"
    ),
  );
  return $plugins;
}
